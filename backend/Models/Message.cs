namespace backend.Models
{
    public class Message
    {
        public string Owner { get; set; }
        public string Text {get; set;}
    }
}