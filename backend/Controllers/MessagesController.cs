using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using backend.Models;

namespace backend.Controllers
{
    [Produces("application/json")]
    [Route("api/messages")]
    [ApiController]
    public class MessagesController : ControllerBase
    {

        static List<Message> messages = new List<Message>()
        {
            new Message()
            {
                Owner = "Atticus",
                Text = "Bork!"
            }
        };

        [HttpGet]
        public IEnumerable<Message> Get()
        {
            return messages;
        }

        [HttpPost]
        public void Post([FromBody] Message message)
        {
            messages.Add(message);
        }
    }
}
