import { Component, OnInit } from '@angular/core';
import { WebService } from './web.service';

@Component({
    selector: 'app-messages',
    template:
        `<div *ngFor="let message of messages">
            <mat-card class="card">
                <mat-card-title>
                    {{message.owner}}
                </mat-card-title>
                <mat-card-content>
                    {{message.text}}
                </mat-card-content>
            </mat-card>
        </div>`
})
export class MessagesComponent implements OnInit {

    messages = [];

    constructor(private webService: WebService) {}

    async ngOnInit() {
        const response = await this.webService.getMessages();
        response.subscribe((res) => {
            this.messages = res;
        });
    }
}
