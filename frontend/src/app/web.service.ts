import { HttpClient } from '@angular/common/http';
import {Injectable} from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class WebService {
    constructor(private http: HttpClient) {

    }

    getMessages(): Observable<any> {
        return this.http.get('https://localhost:5001/api/messages');
    }
}
